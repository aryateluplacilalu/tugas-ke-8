<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Looping</title>
</head>

<body>
	<h1>Berlatih Looping</h1>
	<h3><?= salam("Soal No 1 Looping I Love PHP"); ?></h3>

	<!-- Section PHP -->
	<?php
	function salam($nama)
	{
		return "$nama";
	}

	// Perulangan
	// Looping Ascending
	echo "LOOPING PERTAMA<br>";
	for ($urut = 2; $urut <= 20; $urut += 2) {
		echo "$urut - I Love PHP</br>";
	}
	// Looping Descending
	echo "LOOPING KEDUA<br>";
	for ($urut = 20; $urut >= 2; $urut -= 2) {
		echo "$urut - I Love PHP</br>";
	}

	echo "<h3>Soal No 2 Looping Array Modulo </h3>";

	$numbers = [18, 45, 29, 61, 47, 34];
	echo "array numbers: ";
	print_r($numbers);
	// Lakukan Looping di sini
	// Inisialisasi array $rest untuk menampung sisa bagi dengan angka 5
	$rest = [];
	// Looping untuk mencari sisa bagi dengan angka 5 dari setiap angka dalam array
	foreach ($numbers as $number) {
		$rest[] = $number % 5;
	}

	// Menampilkan array $rest
	echo "<br";
	echo "Array sisa baginya adalah : ";
	echo "<br";
	print_r($rest);

	echo "<h3> Soal No 3 Looping Asociative Array </h3>";
	$items = [
		["id" => "001", "name" => "Keyboard Logitek", "price" => 60000, "description" => "Keyboard yang mantap untuk kantoran", "source" => "logitek.jpeg"],
		["id" => "002", "name" => "Keyboard MSI", "price" => 300000, "description" => "Keyboard gaming MSI mekanik", "source" => "msi.jpeg"],
		["id" => "003", "name" => "Mouse Genius", "price" => 50000, "description" => "Mouse Genius biar lebih pinter", "source" => "genius.jpeg"],
		["id" => "004", "name" => "Mouse Jerry", "price" => 30000, "description" => "Mouse yang disukai kucing", "source" => "jerry.jpeg"]
	];

	foreach ($items as $item) {
		echo "Array (\n";
		echo "    [id] => " . $item['id'] . "\n";
		echo "    [name] => " . $item['name'] . "\n";
		echo "    [price] => " . $item['price'] . "\n";
		echo "    [description] => " . $item['description'] . "\n";
		echo "    [source] => " . $item['source'] . "\n";
		echo ")\n\n";
	}

	echo "<h3>Soal No 4 Asterix </h3>";

	// Looping untuk baris
	for ($i = 1; $i <= 5; $i++) {
		// Looping untuk kolom
		for ($j = 1; $j <= $i; $j++) {
			echo "* ";
		}
		// Pindah ke baris baru setelah setiap baris selesai
		echo "\n";
		echo "<br>";
	}
	?>

	<!-- End Section PHP -->


</body>

</html>